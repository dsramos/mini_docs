package com.vibbra.api.minidocs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
public class Item implements Serializable {

    private static final long serialVersionUID = -1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ativity_id")
    @JsonIgnore
    private Ativity ativity;

    public Item() {
    }

    public Item(Long id, String title, String descricao, Ativity ativity) {
        this.id = id;
        this.title = title;
        this.descricao = descricao;
        this.ativity = ativity;
    }

}
