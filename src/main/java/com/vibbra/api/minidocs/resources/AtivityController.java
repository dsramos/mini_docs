package com.vibbra.api.minidocs.resources;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.dto.AtivityDTO;
import com.vibbra.api.minidocs.dto.ItemDTO;
import com.vibbra.api.minidocs.services.AtivityService;
import com.vibbra.api.minidocs.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value="/api/v1/list")
public class AtivityController {

    @Autowired
    private AtivityService ativityService;

    @Autowired
    private ItemService itemService;

    @GetMapping(value="/{id}")
    public ResponseEntity<Ativity> findAtivity(@PathVariable Long id){

        final Ativity byId = ativityService.findById(id);
        return ResponseEntity.ok(byId);
    }

    @DeleteMapping(value="/{id}")
    public ResponseEntity deleteAtivity(@PathVariable Long id){

        ativityService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<Ativity>> findAll(){

        final List<Ativity> all = ativityService.findAll();
        return ResponseEntity.ok(all);
    }

    @PostMapping
    public ResponseEntity<Ativity> insertAtivity(@RequestBody AtivityDTO ativityDTO){
        final Ativity ativity = ativityService.fromAtivityDTO(ativityDTO);
        final Ativity insert = ativityService.save(ativity);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(ativity.getId()).toUri();
        return  ResponseEntity.created(uri).body(insert);
    }

    @GetMapping(value="/{id}/item")
    public ResponseEntity<List<Item>> findItemByAtivity(@PathVariable Long id){

        final Ativity ativity = ativityService.findById(id);
        return ResponseEntity.ok(ativity.getItens());
    }

    @PostMapping(value="/{id}/item")
    public ResponseEntity<Item> insertItemInAtivity(@PathVariable Long id, @RequestBody ItemDTO itemDTO){
        final Item item = itemService.fromItemDTO(itemDTO);
        ativityService.addItem(id, item);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(item.getId()).toUri();
        return ResponseEntity.created(uri).body(item);
    }

    @PutMapping(value="/{id}/item/{ID}")
    public ResponseEntity<Item> updateItem(@PathVariable Long id, @RequestBody ItemDTO itemDTO, @PathVariable Long ID){
        final Item item = itemService.fromItemDTO(itemDTO);
        itemService.update(ID,item);
        return ResponseEntity.ok(item);
    }

    @DeleteMapping(value="/{id}/item/{ID}")
    public ResponseEntity deleteItem(@PathVariable Long id, @PathVariable Long ID){
        itemService.delete(ID);
        return ResponseEntity.ok().build();
    }

}
