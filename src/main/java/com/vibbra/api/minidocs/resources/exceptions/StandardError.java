package com.vibbra.api.minidocs.resources.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class StandardError implements Serializable {

    private Integer status;
    private String msg;

}
