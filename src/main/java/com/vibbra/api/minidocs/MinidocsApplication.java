package com.vibbra.api.minidocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinidocsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinidocsApplication.class, args);
	}

}

