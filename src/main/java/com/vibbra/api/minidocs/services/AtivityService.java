package com.vibbra.api.minidocs.services;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.dto.AtivityDTO;
import com.vibbra.api.minidocs.repository.AtivityRepository;
import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AtivityService {

    @Autowired
    private AtivityRepository repository;

    @Autowired
    private ItemService itemService;

    public Ativity findById(final Long id){
        final Optional<Ativity> byId = repository.findById(id);
        return byId.orElseThrow(() -> new ObjectNotFoundException("Ativity not found: " + id ));
    }

    public Ativity addItem(final Long id, final Item item){
        final Ativity ativity = findById(id);
        item.setAtivity(ativity);
        itemService.save(item);

        ativity.getItens().add(item);
        return save(ativity);

    }

    public List<Ativity> findAll(){
        final List<Ativity> all = repository.findAll();
        return Optional.ofNullable(all).orElseThrow(() -> new ObjectNotFoundException("Ativities not found!"));
    }

    public Ativity save(final Ativity ativity){
        return repository.save(ativity);
    }

    public void delete(final Long id){
        findById(id);
        repository.deleteById(id);
    }

    public Ativity fromAtivityDTO(final AtivityDTO ativity) {
        return new Ativity(null,ativity.getTitle(),ativity.getDescricao(),new ArrayList<>());
    }


}
