package com.vibbra.api.minidocs.services.exception;

public class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException(final String msg){
        super(msg);
    }

}
