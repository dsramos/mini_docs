package com.vibbra.api.minidocs.services;

import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.dto.ItemDTO;
import com.vibbra.api.minidocs.repository.ItemRepository;
import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    @Autowired
    private AtivityService ativityService;

    public Item findById(final Long id){
        final Optional<Item> byId = repository.findById(id);
        return byId.orElseThrow(() -> new ObjectNotFoundException("Item not found! Id: " + id ));
    }

    public Item save(final Item item){
        return repository.save(item);
    }


    public Item update(final Long id ,final Item item){
        final Item byId = findById(id);
        item.setId(id);
        item.setAtivity(byId.getAtivity());
        return repository.save(item);
    }

    public void delete(final Long id){
        findById(id);
        repository.deleteById(id);
    }

    public Item fromItemDTO(final ItemDTO itemDTO){
        return new Item(null,itemDTO.getTitle(),itemDTO.getDescricao(),null);
    }
}
