package com.vibbra.api.minidocs.repository;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item,Long> {

    List<Item> findItemByAtivity(Ativity ativity);
}
