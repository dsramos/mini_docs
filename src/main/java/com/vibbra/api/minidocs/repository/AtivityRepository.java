package com.vibbra.api.minidocs.repository;

import com.vibbra.api.minidocs.domain.Ativity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AtivityRepository extends JpaRepository<Ativity,Long> {
}
