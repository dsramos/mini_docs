package com.vibbra.api.minidocs.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AtivityDTO implements Serializable {

    private static final long serialVersionUID = -1;

    private String title;

    private String descricao;
}
