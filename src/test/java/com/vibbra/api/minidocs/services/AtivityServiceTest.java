package com.vibbra.api.minidocs.services;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.dto.AtivityDTO;
import com.vibbra.api.minidocs.repository.AtivityRepository;
import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class AtivityServiceTest {

    @InjectMocks
    private AtivityService ativityService;

    @Mock
    private AtivityRepository repository;

    @Mock
    private ItemService itemService;

    private Ativity ativity;
    private AtivityDTO ativityDTO;
    private Ativity ativityWithItem;
    private Item item;

    @Before
    public void setUp() throws IOException {
        ativity = new Ativity(1l,"Teste","descricao teste",null);
        ativityDTO = new AtivityDTO("Teste dto","descricao teste dto");
        item = new Item(1l,"Item","Item teste",ativity);
        ativityWithItem = new Ativity(1l,"Teste","descricao teste",Arrays.asList(item));
    }

    @Test
    public void findById() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.ofNullable(ativity));
        Ativity byId = ativityService.findById(1l);
        Assert.assertEquals("descricao teste" , byId.getDescricao());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void notFound() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.empty());
        ativityService.findById(1l);
    }

    @Test
    public void addItem() {


        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.of(ativity));
        Mockito.when(itemService.save(Mockito.any(Item.class))).thenReturn(item);
        Mockito.when(repository.save(Mockito.any())).thenReturn(ativityWithItem);

        Ativity ativity = ativityService.addItem(1l, new Item());

        Assert.assertNotNull(ativity.getItens());
        Assert.assertEquals(1 , ativity.getItens().size());
        Assert.assertEquals("Item teste" , ativity.getItens().get(0).getDescricao());

    }

    @Test
    public void findAll() {
        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(ativity));
        List<Ativity> all = ativityService.findAll();
        Assert.assertEquals(1,all.size());
        Assert.assertEquals("descricao teste",all.get(0).getDescricao());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void notFoundAll() {
        Mockito.when(repository.findAll()).thenReturn(null);
        ativityService.findAll();
    }

    @Test
    public void save() {
        Mockito.when(repository.save(Mockito.any())).thenReturn(ativity);
        Ativity save = ativityService.save(Mockito.any());
        Assert.assertEquals("descricao teste",save.getDescricao());
        Assert.assertNotNull(save.getId());
    }

    @Test
    public void delete() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.of(ativity));
        ativityService.delete(ativity.getId());
    }

    @Test
    public void fromAtivityDTO(){
        final Ativity ativityReturn = ativityService.fromAtivityDTO(ativityDTO);
        Assert.assertEquals("descricao teste dto",ativityReturn.getDescricao());
    }
}