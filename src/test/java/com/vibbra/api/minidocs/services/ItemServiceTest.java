package com.vibbra.api.minidocs.services;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.dto.ItemDTO;
import com.vibbra.api.minidocs.repository.AtivityRepository;
import com.vibbra.api.minidocs.repository.ItemRepository;
import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;
@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {

    @InjectMocks
    private ItemService itemService;

    @Mock
    private ItemRepository repository;

    private Ativity ativity;
    private Item item;
    private ItemDTO itemDTO;
    private Item itemUpdated;

    @Before
    public void setUp() throws IOException {
        ativity = new Ativity(1L,"Teste","descricao teste",null);
        item = new Item(1l,"Item","Item teste",ativity);
        itemDTO = new ItemDTO("Item DTO","Item teste DTO");
        itemUpdated = new Item(5l,"Item 2","Item Atualizado",ativity);

    }

    @Test
    public void findById() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.ofNullable(item));
        Item byId = itemService.findById(1l);
        Assert.assertEquals("Item teste",byId.getDescricao());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void notFound() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.empty());
        itemService.findById(1l);
    }

    @Test
    public void save() {
        Mockito.when(repository.save(Mockito.any())).thenReturn(item);
        Item save = itemService.save(Mockito.any());
        Assert.assertEquals("Item teste",save.getDescricao());
        Assert.assertNotNull(save.getId());
    }

    @Test
    public void update() {
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.ofNullable(item));
        Mockito.when(repository.save(Mockito.any())).thenReturn(itemUpdated);
        Item save = itemService.update(5l,new Item());
        Assert.assertEquals("Item Atualizado",save.getDescricao());
        Assert.assertNotNull(save.getId());
    }

    @Test
    public void delete(){
        Mockito.when(repository.findById(Mockito.any())).thenReturn(Optional.ofNullable(item));
        itemService.delete(item.getId());
    }

    @Test
    public void fromItemDTO(){
        final Item item = itemService.fromItemDTO(itemDTO);
        Assert.assertEquals("Item teste DTO",item.getDescricao());
    }
}