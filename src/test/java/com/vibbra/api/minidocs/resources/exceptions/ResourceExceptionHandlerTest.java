package com.vibbra.api.minidocs.resources.exceptions;

import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RunWith(MockitoJUnitRunner.class)
public class ResourceExceptionHandlerTest {

    @InjectMocks
    private ResourceExceptionHandler resourceExceptionHandler;

    @Test
    public void objectNotFound() {
        final ResponseEntity<StandardError> response = resourceExceptionHandler.objectNotFound(new ObjectNotFoundException("Erro"));
        Assert.assertEquals(NOT_FOUND,response.getStatusCode());
        Assert.assertEquals("Erro",response.getBody().getMsg());
        Assert.assertEquals(404,response.getBody().getStatus(),0);
    }
}