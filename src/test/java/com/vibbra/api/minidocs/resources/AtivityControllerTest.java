package com.vibbra.api.minidocs.resources;

import com.vibbra.api.minidocs.domain.Ativity;
import com.vibbra.api.minidocs.domain.Item;
import com.vibbra.api.minidocs.resources.exceptions.ResourceExceptionHandler;
import com.vibbra.api.minidocs.services.AtivityService;
import com.vibbra.api.minidocs.services.ItemService;
import com.vibbra.api.minidocs.services.exception.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class AtivityControllerTest {
    private static final String EMPTY_REQUEST = "{}";
    public static final String API_V1_LIST = "/api/v1/list";

    @InjectMocks
    private AtivityController ativityController;

    @Mock
    private AtivityService ativityService;

    @Mock
    private ItemService itemService;

    private MockMvc mockMvc;
    private Ativity ativity;
    private Item item;
    @Before
    public void setUp() {
        mockMvc = standaloneSetup(ativityController).setControllerAdvice(new ResourceExceptionHandler()).build();
        item = new Item(1l,"Item","Item teste",null);
        ativity = new Ativity(1l,"Teste","descricao teste",Arrays.asList(item));
    }

    @Test
    public void addNewAtivity() throws Exception {
        final String emptyItemsRequest = "{\"title\":\"teste\", \"descricao\":\"testeDescricao\"}";

        when(ativityService.fromAtivityDTO(any())).thenReturn(ativity);
        when(ativityService.save(any())).thenReturn(ativity);

        // WHEN call cart controler
        mockMvc.perform(post(API_V1_LIST )
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON)
                .content(emptyItemsRequest))
                .andExpect(status().isCreated());
    }

    @Test
    public void findAtivity() throws Exception {

        when(ativityService.findById(Mockito.any())).thenReturn(ativity);

        // WHEN call cart controler
        mockMvc.perform(get(API_V1_LIST+"/{id}",1 )
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    public void notFindAtivity() throws Exception {

        when(ativityService.findById(Mockito.any())).thenThrow(new ObjectNotFoundException("Ativity not found"));

        mockMvc.perform(get(API_V1_LIST+"/{id}",1 )
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }


    @Test
    public void findAllAtivity() throws Exception {

        when(ativityService.findAll()).thenReturn(Arrays.asList(ativity));

        // WHEN call cart controler
        mockMvc.perform(get(API_V1_LIST )
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    public void findItemByAtivity() throws Exception {

        when(ativityService.findById(Mockito.any())).thenReturn(ativity);

        // WHEN call cart controler
        mockMvc.perform(get(API_V1_LIST+"/{id}/item",1)
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
    }

    @Test
    public void addNewItem() throws Exception {
        final String emptyItemsRequest = "{\"title\":\"teste\", \"descricao\":\"testeDescricao\"}";

        when(ativityService.addItem(Mockito.any(), Mockito.any())).thenReturn(ativity);
        when(itemService.fromItemDTO(Mockito.any())).thenReturn(item);

        // WHEN call cart controler
        mockMvc.perform(post(API_V1_LIST+"/{id}/item",1)
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON)
                .content(emptyItemsRequest))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateItem() throws Exception {
        final String emptyItemsRequest = "{\"title\":\"teste\", \"descricao\":\"testeDescricao\"}";

        when(itemService.update(Mockito.any(), Mockito.any())).thenReturn(item);
        when(itemService.fromItemDTO(Mockito.any())).thenReturn(item);

        // WHEN call cart controler
        mockMvc.perform(put(API_V1_LIST+"/{id}/item/{ID}",1,1)
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON)
                .content(emptyItemsRequest))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteItem() throws Exception {

        mockMvc.perform(delete(API_V1_LIST+"/{id}/item/{ID}",1,1)
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteAtivity() throws Exception {

        mockMvc.perform(delete(API_V1_LIST+"/{id}",1)
                .headers(new HttpHeaders())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}